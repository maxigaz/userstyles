# Userstyles made by maxigaz

This repo contains various userstyles made for various websites. They were written in the [usercss](https://github.com/openstyles/stylus/wiki/Usercss) format, specifically for [Stylus](https://add0n.com/stylus.html), but they might be compatible with other browser addons such as [xStyle](https://github.com/FirefoxBar/xStyle).

## List of maintained userstyles

(The repositories listed below include screenshots and notes relevant to the style in question.)

Name                                              | Repository
-----                                             | -----------------------
Mozilla Add-ons (AMO) Dark                        | [Codeberg repo](https://codeberg.org/maxigaz/amo-dark) [GitLab repo](https://gitlab.com/maxigaz/amo-dark)
Searx Solarized                                   | [Codeberg repo](https://codeberg.org/maxigaz/searx-solarized) [GitLab repo](https://gitlab.com/maxigaz/searx-solarized)

## Mirrors

You can also find my work on [UserStyles.world](https://userstyles.world/user/maxigaz).

## Miscellaneous tweaks

Other visual tweaks not directly related to Usercss or Stylus can be found in the [Snippets](https://gitlab.com/maxigaz/userstyles/snippets) page. (See the description of each one on how to use them.)
